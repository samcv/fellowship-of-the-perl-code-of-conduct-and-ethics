# Conduct and Ethics Pledge of the Fellowship of the Perl

As a Member of The Fellowship of the Perl,
I pledge to uphold and defend the values
of Excellence, Integrity, Respect, and Safety in my relationship to the Perl
community. I will conduct myself honestly and ethically in all my in-person and
digital actions, in accordance with this pledge of Conduct and Ethics.

## Excellence

* I will always strive for technical excellence in my work and interactions
   with the Perl community.
* I will seek to be a life-long learner within the community.
* I will share my knowledge with others, as much as I am able.
* I will always strive to make my presentations and prepared statements as
   effective, informative, and enjoyable as my abilities will permit.

## Integrity

* I will always be honest in my expression of my capabilities and limitations,
   and of the capabilities and limitations of software that I write, maintain,
   and/or use.
* I will not engage in harmful, untruthful gossip.
* I will speak truthfully, and present evidence to support my opinions and
   works.
* I will not represent myself as speaking for the whole community, nor any
  subset of the community, unless specifically authorized to do so.
* I will transparently disclose any conflicts of interest or conflicting motives
  for my code and interactions with Perl and the Perl community.
* I will be transparent in my dealings with the Perl community: avoiding hidden
  or secret conversations affecting the community whenever possible.
* I will respect the privacy of individuals, when possible, while remembering
  that behavior which is damaging to others or the community is not often not
  deserving of the same protections.

## Respect

* I will not engage in physical, verbal, or digital harassment of any person or
  group of persons of any kind, including but not limited to physical violence,
  racist, sexist, ableist, homophobic or transphobic language, threats or
  intimidation, degradation or demeaning of any person or class of people
  through words, actions or imagery, doxxing, deliberate misgendering or
  deadnaming of a transgender person, unwanted intimate attention or undesired
  physical or digital contact.
* I will strive to make discussions and events in which I participate and the
  community at large more inclusive, by pointing out and working to remedy
  language, works, and cultural and structural problems that may limit
  people’s participation in the community.
* When participating in discussions, I will evaluate proposals and works on
  their merits, refraining from _ad hominem_ or other arguments that distract
  from the discussion.

## Safety

* I will ensure, whenever possible, that I am welcoming and friendly to all
  members of the Perl community, with affirmation and encouragement, including
  members of traditionally-marginalized groups of people and new members within
  the community.
* I will not permit other members of the community to engage unchecked in
  physical, verbal, or digital harassment of any person or group of persons, if
  it is within my power to prevent them from doing so.
* I will make myself available, as much as possible and within my ability, to
  assist community members in resolving conflicts through mediation.
* I will make myself available, in the event that a community member feels
  physically or psychologically unsafe, so that they may come to me with their
  concerns, and I will willingly serve as champion for them, or assist them in
  finding a champion, to find a way to alleviate their unsafety.

I do hereby subscribe myself to this Conduct and Ethics Pledge, and affirm that
I will abide by it, and will not waver from its values under any pressure
and/or incentive.
